// The root URL for the RESTful services
var rootURL = "http://localhost:8080/amarcasrest/api/productos";

var currentProducto;

// Retrieve product list when application starts 
findAll();

// Nothing to delete in initial application state
$('#btnDelete').hide();
$('#btnSave').hide();

// Register listeners
$('#btnSearch').click(function() {
	search($('#searchKey').val());
	return false;
});

// Trigger search when pressing 'Return' on search key input field
$('#searchKey').keypress(function(e){
	if(e.which == 13) {
		search($('#searchKey').val());
		e.preventDefault();
		return false;
    }
});

$('#btnAdd').click(function() {
	newproducto();
        $('#btnUpdate').hide();
        $('#btnSave').show();
	return false;
});

$('#btnSave').click(function() {
    addProducto();
    return false;
});

$('#btnUpdate').click(function() {
    updateProducto();
    return false;
});


$('#btnDelete').click(function() {
	deleteProducto();
	return false;
});

$('#productoList a').live('click', function() {
	findById($(this).data('identity'));
});

function search(searchKey) {
	if (searchKey == '') 
		findAll();
	else
		findByName(searchKey);
}

function newproducto() {
	$('#btnDelete').hide();
	currentProducto = {};
	renderDetails(currentProducto); // Display empty form
}

function findAll() {
	console.log('findAll');
	$.ajax({
		type: 'GET',
		url: rootURL,
		dataType: "json", // data type of response
		success: renderList
	});
}

function findByName(searchKey) {
	console.log('findByName: ' + searchKey);
	$.ajax({
		type: 'GET',
		url: rootURL + '/search/' + searchKey,
		dataType: "json",
		success: renderList 
	});
}

function findById(id) {
	console.log('findById: ' + id);
	$.ajax({
		type: 'GET',
		url: rootURL + '/' + id,
		dataType: "json",
		success: function(data){
			$('#btnDelete').show();
                        $('#btnUpdate').show();
                        $('#btnSave').hide();
			console.log('findById success: ' + data.nomProducto);
			currentProducto = data;
			renderDetails(currentProducto);
		}
	});
}

function addProducto() {
	console.log('addProducto');
	$.ajax({
		type: 'POST',
		contentType: 'application/json',
		url: rootURL,
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('Producto creado exitosamente');
			$('#btnDelete').show();
			$('#codproducto').val(data.id);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('error creando producto: ' + textStatus);
		}
	});
}

function updateProducto() {
	console.log('updateProducto');
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: rootURL,// + '/' + $('#codproducto').val(),
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('Producto actualizado exitosamente');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error actualizando: ' + textStatus);
		}
	});
}

function deleteProducto() {
	console.log('deleteProducto');
	$.ajax({
		type: 'DELETE',
		url: rootURL + '/' + $('#codproducto').val(),
		success: function(data, textStatus, jqXHR){
			alert('Producto eliminado exitosamente');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error eliminando producto');
		}
	});
}

function renderList(data) {
	// JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
	var list = data == null ? [] : (data instanceof Array ? data : [data]);

	$('#productoList li').remove();
	$.each(list, function(index, producto) {
	console.log(producto.codigo_producto);	
            $('#productoList').append('<li><a href="#" data-identity="' + producto.codigoProducto + '">'+ producto.nomProducto +'</a></li>');
	});
}

function renderDetails(producto) {
	$('#codproducto').val(producto.codigoProducto);
	$('#name').val(producto.nomProducto);
        $('#talla').val(producto.talla);
        $('#cantidad').val(producto.cantidad);
        $('#valor').val(producto.valor);
}

// Helper function to serialize all the form fields into a JSON string
function formToJSON() {
	var codproducto = $('#codproducto').val();
        console.log(JSON.stringify({
		"codigoProducto": codproducto == "" ? null : codproducto, 
		"nomProducto": $('#name').val(),
                "talla": $('#talla').val(),
                "cantidad": $('#cantidad').val(),
                "valor": $('#valor').val()
		}));	
	return JSON.stringify({
		"codigoProducto": codproducto == "" ? null : codproducto, 
		"nomProducto": $('#name').val(),
                "talla": $('#talla').val(),
                "cantidad": $('#cantidad').val(),
                "valor": $('#valor').val()
		});
}
