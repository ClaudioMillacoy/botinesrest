package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "mar")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MarClase.findAll", query = "SELECT m FROM MarClase m")
    , @NamedQuery(name = "MarClase.findById", query = "SELECT m FROM MarClase m WHERE m.id = :id")
    , @NamedQuery(name = "MarClase.findByNombreMarca", query = "SELECT m FROM MarClase m WHERE m.nombreMarca = :nombreMarca")})
public class MarClase implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombre_marca")
    private String nombreMarca;

    public MarClase() {
    }

    public MarClase(Integer id) {
        this.id = id;
    }

    public MarClase(Integer id, String nombreMarca) {
        this.id = id;
        this.nombreMarca = nombreMarca;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombreMarca() {
        return nombreMarca;
    }

    public void setNombreMarca(String nombreMarca) {
        this.nombreMarca = nombreMarca;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MarClase)) {
            return false;
        }
        MarClase other = (MarClase) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.MarClase[ id=" + id + " ]";
    }
    
    
}
