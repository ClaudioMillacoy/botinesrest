package root.services;

import com.google.gson.Gson;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.persistence.entities.ProductoClase;


@Path("/productos")

public class ClienteRest {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("Per_marcas");
    EntityManager em;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {
        em = emf.createEntityManager();
        List<ProductoClase> lista = em.createNamedQuery("ProductoClase.findAll").getResultList();
        em.close();
        return Response.ok().entity(new Gson().toJson(lista)).build();
    }
    
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") int idbuscar) {
        em = emf.createEntityManager();
        ProductoClase Producto = em.find(ProductoClase.class,idbuscar);
        em.close();
        return Response.ok(500).entity(new Gson().toJson(Producto)).build();
    }
    
    @GET
    @Path("/search/{namebuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("namebuscar") String namebuscar) {
        em = emf.createEntityManager();
        Query q=em.createNamedQuery("ProductoClase.findByNomProducto");
        q.setParameter("nomProducto", namebuscar);
        List<ProductoClase> lista = q.getResultList();
        em.close();
        return Response.ok().entity(new Gson().toJson(lista)).build();
    }
    
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response nuevo (ProductoClase Productonuevo) {
        em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(Productonuevo);
        em.getTransaction().commit();
        em.close();
        return Response.ok().entity(new Gson().toJson(Productonuevo)).build();
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response actualizar(ProductoClase ProductoUpdate) {
        em = emf.createEntityManager();
        em.getTransaction().begin();
        ProductoUpdate = em.merge(ProductoUpdate);
        em.getTransaction().commit();
        em.close();
        return Response.ok().entity(new Gson().toJson(ProductoUpdate)).build();
    }

    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarId(@PathParam("ideliminar") int ideliminar) {
        em = emf.createEntityManager();
        em.getTransaction().begin();
        //Query q=em.createNamedQuery("ProductoClase.deleteByNomProducto");
        //q.setParameter("codigo_producto", ideliminar);
        ProductoClase Producto = em.find(ProductoClase.class,ideliminar);
        if(Producto != null){
            em.remove(Producto);
        }
//        q.executeUpdate();
        em.getTransaction().commit();
        em.close();
        return Response.ok().build();
    }
    
    @PreDestroy
    public void destruct()
        {
         emf.close();
        }
}
